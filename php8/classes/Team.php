<?php
/**
 * Created by: Stephan Hoeksema 2021
 * sandbox
 */

class Team
{
    protected $title;
    protected $members = [];
    /**
     * Team constructor.
     * @param $title, title of the team.
     * @param array $members, members of the team.
     */
    public function __construct($title, $members = [])
    {
        $this->title = $title;
        $this->members = $members;
    }

    /**
     * @param ...$params
     * @return static
     */
    public static function start(...$params)
    {
        return new static(...$params);
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @param $name
     */
    public function add($name)
    {
        $this->members[] = $name;
    }

    /**
     * @return array
     */
    public function members()
    {
        return $this->members;
    }
}



