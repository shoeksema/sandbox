<?php
/**
 * Created by: Stephan Hoeksema 2021
 * sandbox
 */

class Kitebrands
{
    protected $kitebrands = [];

    /**
     * @param array|mixed|string[] $kitebrands
     */
    public function setKitebrands($kitebrands)
    {
        $this->kitebrands = $kitebrands;
    }

    /**
     * @return array
     */
    public function getKitebrands()
    {
        return $this->kitebrands;
    }



}