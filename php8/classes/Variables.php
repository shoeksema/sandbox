<?php
/**
 * Created by: Stephan Hoeksema 2021
 * sandbox
 */

class Variables {


    public $teacher = "";
    private $numYears = "";

    public function __construct($teacher)
    {
        $this->greeting = "Welkom ADSD 2021";
        $this->teacher = $teacher;

    }

    public function strings()
    {
        return "$this->greeting, de docent voor deze les is $this->teacher!";
            //. "<p>" . htmlspecialchars($_GET['name']) . "</p>";

    }

    public function exameDate()
    {
        //Calculation until exam date in days
        $exameDate = new DateTime("Nov 15, 2021 10:38:22 GMT");
        $today = new DateTime('',new DateTimeZone('Europe/London'));

        return $today->diff($exameDate)->days . " days to DOMP exam";
    }

    public function startCsharpeDate()
    {
        //Calculation until exam date in days
        $startDate = new DateTime("Feb 6, 2022 10:38:22 GMT");
        $today = new DateTime('',new DateTimeZone('Europe/London'));

        return $today->diff($startDate)->days . " days to start C#";
    }
}
    