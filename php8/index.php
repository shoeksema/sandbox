<?php
echo "<hr><h1>FUNCTIONS</h1><hr>";
/**
 * De kracht van PHP komt van de manier hoe functie gebruikt worden.
 * Naast dat er meer dan 1000 built-in functions zijn kan je ook zelf
 * de functions maken.
 *
 * De built-in functions kunnen altijd direct gebruikt worden. De totale
 * lijst met built-in functions kan je hier vinden: https://www.w3schools.com/Php/php_ref_overview.asp
 */
/*************/
echo "<hr><h1>LOOPS</h1><hr>";
/**
 * while - loops door een stuk  code zolang de specifieke conditie waar is
 * do...while - loops door een stuk  code eenmaal en herhaalt zich zolang
 * de conditie die gesteld is waar is.
 * for - loops loops door een stuk  code een specifiek aantal keer
 * foreach - loops door een stuk  code op basis van een array
 */
echo "--- while ---";
echo "<br>";
$age = 46;
while ($age <= 50) {
    echo "Geen zorgen zo lang ik onder de 50 ben mijn leeftijd is nu $age<br>";
    $age++; //conditie
}
echo "<br>";
echo "--- do ... while ---";
echo "<br>";
$age_wife = 36;
do {
    echo "Geen zorgen zo lang ze onder de 50 is, leeftijd vrouw $age_wife<br>";
    $age_wife++; //conditie
} while ($age_wife <= 50);
echo "<br>";
echo "--- for ---";
echo "<br>";
//$ytd = date('Y');
//die(var_dump($ytd));
for($x = 2015; $x < date('Y'); $x++ ) {
    echo "my daugther was in $x " . $x - 2015 .  " years old!<br>";
}
echo "<br>";
echo "--- foreach ---";
echo "<br>";
$kitebrands = ['Airrush', 'Naish', 'Gin', 'Gaastra'];
foreach ($kitebrands as $kitenbrand) {
    echo "$kitenbrand is great!<br>";
}
echo "<br>";
echo "--- foreach with conditional---";
echo "<br>";
foreach ($kitebrands as $kitenbrand) {
    if($kitenbrand == 'Gin') { //conditional if()
        echo "$kitenbrand is great!<br>";
    } else {
        echo "$kitenbrand is good!<br>";
    }
}

echo "<hr><h1>IF...ELSEIF...ELSE</h1><hr>";
/**
 * Conditional statements, met een conditional worden er verschillende
 * acties ondernomen gebaseerd op de condities die gegeven zijn.
 * Wanneer een waarde waar is wordt een if uitgevoerd.
 *
 * Vaak schrijven code die alleen uitgevoerd wordt wanneer er iets waar
 * of niet waar is. De volgende conditie hebben:
 * if - voert code uit wanneer een conditie waar is.
 * if ... else wordt voert code uit wanneer een conditie waar is en een
 *             ander stuk code wanneer het niet waar is.
 * if..else..elseif verschillende stukken code die uitgevoerd worden
 *                  op basis van drie condities.
 * switch statement wordt gebruikt wanneer er meer dan drie condities zijn.
 */
echo "--- if ---";
echo "<br>";
//Wanneer(if) het vroeger is dan 16:00 een(code) fijne dag wensen
$myTime = date('h'); //alleen de uren
if($myTime < 16) {
    echo "Een fijne dag!";
}

/**
 * Bij een else is het later dan 17 en dan wensen we iemand een fijne avond.
 */
echo "<hr>";
echo "--- if...else ---";
echo "<br>";
// (if)   wanneer het vroeger dan 1600 is dan fijne dag
if($myTime < 17){
    echo "Een fijne dag!";
} else {// (else) na 16:00 fijne avond
    echo "Fijne avond!";
}
echo "<hr>";
echo "--- if...elseif...else ---";
echo "<br>";
// (if)   wanneer het vroeger dan 1000 is dan 'Goedemorgen'
if($myTime < 10){
    echo "$myTime ";
    echo "Goedmorgen!";
} elseif($myTime < 17) {// (elseif) voor 17:00 fijne dag
    echo "Fijne dag!";
} else { //

}

/** Multidimensional arrays - array in een array
 * De waarde van een array is een array.
 *
 * $cars = [
 * ['BMW', 3, 10 ],
 * ['Audi', 2, 5 ],
 * ['Kia', 5, 6 ]
 * ];
 *
 */
echo "<hr><h1>MULTIDIMENSIONAL ARRAY</h1><hr>";

$cars = [
    ['BMW', 3, 10],
    ['Audi', 2, 5],
    ['Kia', 5, 6]
];

echo "Van de " . $cars[1][0] . " zijn er " . $cars[1][1] . " op voorraad en " . $cars[1][2] . " verkocht!";

/** Opdracht
 * Maak een multidimensional array van de docenten van de ADSD met hun geboortejaar, hobby, module(s).
 * Docenten van de ADSD Stephan, Rudy, Siwa, Mounia en Fake.
 */
echo "<br>";

echo "<hr>Opdracht multidimensional array<hr>";
$teachers = [
    [
        'Stephan' => [
            'dob' => date("1976/04/15"),
            'hobby' => 'Kitesurfing',
            'module' =>'DOMP']
    ],
    [
        'Rudy' => [
            'dob' => date('1985/11/06'),
            'hobby' => 'Profielen maken',
            'module' => 'C#'
        ]
    ]
];
$mydate = strtotime($teachers[0]['Stephan']['dob']);

echo "<pre>" . var_dump(date("jS M Y", $mydate)) . "</pre>";

$myName = $teachers[0];
$array = array_keys($myName);
echo "<br>";
echo array_shift($array);
echo "<br>";


/** Associative arrays - Arrays met named keys
 * Als we nu de waarde van een array willen laten zien gebruiken we de waarde die afgegeven wordt
 * door de zore bades indes. Een nummer.
 *
 * Het is ook mogelijk om de key een naam te geven.
 * Een array kunnen we op meerdere manieren maken:
 * $cars = array('BMW', 'Audi', 'Kia');
 * of
 * $cars = ['BMW', 'Audi', 'Kia'];
 *
 * De waardes van de associative array zijn gebaseerd op een 'key' => 'value'.
 * Dit houdt in dat de waarde in de array benaderd kunnen op basis van de gegevenb 'key'.
 *
 * We kunnen ook:
 * $cars = [
 *      'favorite' => 'BMW',
 *      'want' => 'Audi',
 *      'driving' => 'Kia'
 * ];
 *
 * echo $cars['want'];
 */
echo "<hr><h1>ASSOCIATIVE ARRAY</h1><hr>";
$cars = [
    'favorite' => 'Spijker',
    'want' => 'Audi',
    'driving' => 'Kia'
];
var_dump($cars);
echo "<br>";
echo $cars['want'];
echo "<br>";
/**
 * Met deze inline functie kunnen we zien of er iets aanwezig is.
 */
var_dump(in_array('Audi', $cars)); // returns a boolean!
echo "<br>";




/** ARRAY
 * Een array is verzameling van waardes die onder een variabele vallen.
 * Wanneer we met een database gaan werken kunnen we bijvoorbeeld zeggen:
 * laat alle automerken zien. Die we dan in een variabele $cars stoppen.
 *
 * Een array kunnen we op meerdere manieren maken:
 * $cars = array('BMW', 'Audi', 'Kia');
 * of
 * $cars = ['BMW', 'Audi', 'Kia'];
 *
 * De waardes van de array zij gebaseerd op een 'zero-based index'.
 * Dit houdt in dat de waarde in de array beginnen met tellen op 0, 1, 2....
 * Dus om de tweede waarde te zien vragen we 1 op!
 */
echo "<hr><h1>ARRAY</h1><hr>";

$cars = array('BMW', 'Audi', 'Kia');
var_dump($cars);
echo "<br>";
$autos = ['BMW', 'Audi', 'Kia'];
var_dump($autos);
echo "<br>";
var_dump($cars[1]);
echo "<br>";
var_dump($autos[0]);

/** Opdracht
 * Maak een array met de namen van vijf IDE's. Laat de derde IDE op het scherm zien.
 * Gebruik count() als inline function, gebruik een inline function om het laatste
 * element van een array te verwijderen
 */
echo "<br>";
echo "<hr>Opdracht array<hr>";
$ides = ['phpStorm', 'Visual Stude Code', 'Notepad++', 'Netbeans', 'Atom'];
var_dump($ides[2]);
echo "<br>";
echo count($ides);
echo "<br>";
var_dump($ides);
echo "<br>";
array_pop($ides);
var_dump($ides);

/** BOOLEAN
 * Een boolean is een waarde die true of false is of 1 of 0. Wanneer we over
 * conditionals gaan praten word dit snel veel duidelijker.
 */
echo "<hr><h1>BOOLEAN</h1><hr>";

$true_false = true;
var_dump(is_bool($true_false));


/** INTEGER
 * Integer data type is een non-decimal(geen punt amerikaans!!) nummer tussen -2,147,483,648 en 2,147,483,647.
 *
 * Regels voor integers:
 *
 * Een integer moet minimaal een nummer hebben
 * Een integer mag geen decimaal punt hebben (1.00 Let op amerikaans!)
 * Een integer Kan positief of negatief zijn
 *
 * In de volgende voorbeelden is $x een integer. De PHP var_dump() functie geeft het
 * data type en de waarde(value) terug:
 */
echo "<hr><h1>INTEGER</h1><hr>";

$x = 10;

var_dump($x);

/**
 * Er zijn ook built in functions om te checken of het een integer is.
 */
echo "<br>";
var_dump(is_int($x));

$y = 5.5;
echo "<br>";
var_dump(is_int($y));
/** FLOAT
 * Float is een numeriek getal met een decimal point(1.00)
 * (Bij ons schrijven is dat een comma(1,00))
 *
 * Of het is een nummer in exponentiële vorm.
 * In de wetenschappelijke notatie wordt een getal weergegeven in
 * exponentiële notatie, waarbij een deel van het getal wordt
 * vervangen door E+n,waarbij E (exponent) het voorgaande getal
 * met 10 vermenigvuldigt tot de macht n th. Een wetenschappelijke
 * notatie met twee decimalen geeft bijvoorbeeld 12345678901 weer
 * als 1,23E+10, wat 1,23 keer 10 is tot de 10e macht.
 */

echo "<hr><h1>FLOAT</h1><hr>";

$z = 10.934;

var_dump($z);

// inline function of het een float is.
echo "<br>";
var_dump(is_float($z));


/**
 * Opdracht rekenen met integer en float
 * 1. sommen met +, -, / en * (integer en float)
 * 2. Geef een random nummer tussen de 1 en 6 (dobbelstenen / afronden op hele getallen)
 * 3. Welke inline functions zijn er voor het rekenen geef twee voorbeelden
 */

/**
 * PHP ondersteund de volgende data types:
 * String
 * Integer
 * Float (floating point numbers - also called double)
 * Boolean
 * Array
 * Object
 * NULL
 * Resource
 *
 * Variabelen in PHP zijn case sensitive ($a is niet $A)
 *
 * Om code 'uit' te zetten gebruiken we comments // of /*
 * PHP heeft veel built in functions die we gebruiken tijdens deze
 * code sessies zal ik verschillende laten zien en in de opdrachten
 * wordt er ook gevraagd om andere built-in functions te gebruiken.
 */

/** String
 * Een sting is een serie characters zoals 'Hallo ADSD 2021'.
 * De sting kan worden omringd met een single(') of double(") quotes.
 * Het verschil tussen deze twee moet je met programmeren zien!
 */
echo "<hr><h1>STRING</h1><hr>";
/** string voorbeeld */

$adsd = "ADSD 2021";

$greeting = "Welkom $adsd"; //output Hello ADSD 2021
/** Verschil singel/double quotes */
//$greeting = 'Welkom $adsd'; //output Hello ADSD 2021

echo $greeting;
echo "<br>";
// built in function strlen() string length
echo strlen($greeting);

/** Opdracht strings
 * 1. Maak een variabele waarin je naam zet.
 * 2. Maak een variabele met je geboortestad.
 * 3. echo je naam en geboortestad op het scherm.
 * 4. Zoek 4 inline functie voor stings en laat daarbij voorbeelden zien.
 */


