<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Auto refresh   -->
    <meta http-equiv="refresh" content="10">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title><?= $app->teacher; ?></title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
    </div>
</div>
<div class="card-group">
    <?php
        foreach ($ADSD->members() as $teacher) {
            echo
                '<div class="card m-4" style="width: 6rem;">
                    <img src="/views/img/'.$teacher.'.jpg" class="card-img-top figure-img img-fluid rounded" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">' . $teacher . '</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa distinctio eos </p>
                    </div>
                </div>';
        }
    ?>


<!-- Optional JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
        crossorigin="anonymous"></script>
</body>
</html>