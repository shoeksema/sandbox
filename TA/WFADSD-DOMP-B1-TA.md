# Samenwerkingsovereenkomst

> 8 September 2020 - V1

**Naam Partijen:** Carlijn den Hartog, Ghislaine El Fardaoussi, Safouane Messaoudi, Samir Mokiem, Stijn Engelmoer

## Huisregels
Progress updates via discord, teams of de mail. Dan zien we dat je er aan werkt in plaats van op het laatst te horen krijgen dat je niks hebt gedaan (in de daily stand ups)

* Via welke kanalen hebben we contact met elkaar?
    * Onze contacten verlopen altijd via de app, Teams, Discord, GitLab of fysiek. De mail, Discord en Gitlab gebruiken we voor overdracht van documenten en code. Ben je verhinderd, dan geef je dit zo snel mogelijk door via de app, bellen of sms. 

> **Heel belangrijk:** altijd reageren wanneer er belangrijke vragen of mededelingen in de groepsapp zijn gedaan. Alleen maar lezen en niet reageren is niet respectvol naar het team. Hier zijn ook consequenties aan verbonden. 

* Wanneer zien we elkaar? Hoe laat en op welke dagen?
    * Dinsdag en woensdag zijn onze vaste project werk dagen. De tijd wordt telkens vastgelegd via de whatsapp groepsapp. Daily standups hebben we elke dag (~ 30 min). Hierin laten we aan elkaar zien hoe ver we zijn gekomen en wat we hebben gemaakt (Progress update)

* Wanneer hebben we een overlegmoment?
  * Elke werkdag (maandag t/m vrijdag) hebben we een daily standup. De exacte tijdstippen bespreken we via de app. Normaalgesproken zouden we fysiek een standup houden, maar ivm covid19, houden we de standups tijdelijk via Teams. Wanneer het weer mogelijk is, houden we onze dagelijkse fysieke standups in de bibliotheek of op school  

* Hoe leggen we onze meetings/afspraken vast?
    * Eerst spreken we met elkaar af via de groepsapp. Via de agenda van het student-mailadres(De Calendar) worden de afspraken vervolgens vastgelegd. De meetings zelf verlopen dan via Teams (tenzij wij elkaar fysiek spreken natuurlijk).

* Wat als je iets niet inlevert?
    * Eerst vragen we je om een goede reden. Als je iets niet inlevert wat een teamopdracht is dan krijg je een slag erbij. Als er de dag van te voren wordt gemeld dat je bij de eerstvolgende standup het werk niet af hebt, dan wordt dit als prima gezien mits je de standup daarna maar wel het geplande werk af hebt.

* Hoe en wanneer lever je eigen opdracht in bij het team?
  * Het liefste minimaal 1-2 dagen van te voren de opdrachten inleveren. Dit geeft ons ruimte mocht er iets misgaan of als wij nog ergens hulp bij nodig hebben van de docenten. Het inleveren van je werk doe je via Git. Daarna komt het team bij elkaar via een Teamsmeeting bijvoorbeeld om elkaars werk te bekijken en eventueel samen te voegen. 

* Wat gebeurt er als je een afspraak hebt met bijvoorbeeld de tandarts?
    * Heb je een doktersafspraak etc tijdens onze uren samen, geef dat dan zelf tijdig aan. Het liefste minimaal 1-2 dagen van te voren mits dit mogelijk is. Doorgeven gaat eerst via whatsapp of anders via bellen/of sms'en. Ook je werkrooster geef je door als je die binnen hebt.

* Wat als iemand niet komt opdagen?
  * _**Zonder goede reden**_ gaan we werken met de vier slagen-regel.

## Basisregels
* Probeer, als het mogelijk is, zo snel als je kan te reageren in de groepsapp (ADSD Groep B1). Als je de berichten leest, reageer dan. Mocht je het hebben gelezen, maar kan je niet meteen reageren, geef dit dan aan. Bijvoorbeeld met een berichtje “Ik reageer straks.”
* Iedereen zijn input is belangrijk en wordt gevalideerd.
* Respecteer elkaar en elkaars meningen.
* Als je een probleem hebt geef dit dan aan. (Persoonlijk of met betrekking tot een project)
* Kom op tijd bij de meetings.

## De vier slagenregels
Slag | Vervolg
------------ | -------------
1 | Je krijgt het voordeel van de twijfel. We spreken elkaar erop aan
2 | Je krijgt een groepswaarschuwing. We spreken af dat dit gedrag niet meer voorkomt
3 | We betrekken de docent erbij. Die maakt afspraken met de leerling en het team
4 | De docent neemt maatregelen