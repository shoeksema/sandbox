<br />
<br />

# Teamafspraken - groep C3 

#### Aman Maksoedan, Chevelly Somopawiro, Edwin Mulder, Hermijn Verbaan, Jamal Arnhem
<br />
<br />

## Inhoud.

### -Contactmomenten.                     
### -Wanneer is de daily?
### -Gevolgen niet voltooide taken en afwezigheid.
### -Afmelden en ziekmelden.
### -Onderlingen afspraken
### -Contact.

<br />

- - - 

## Contactmomenten.

<br />

### Online meeting:
Maandag en Vrijdag is er een standaard meeting, begintijd tussen 12:30-13:00 uur. Reden, als er eventueel vragen of obstakels zijn, waar je in de loop van de week of weekend tegen aan bent gelopen, kunnen deze gelijk meegenomen en besproken worden. Verder zijn deze momenten bedoeld om onderlinge afspraken te maken of om projecten te bespreken. Het team kan tijdens deze meetings ook gebruik maken van scherm overname of scherm delen. Platforms die gebruikt worden bestaan uit Teams, Discord. 

<br />

### Fysieke meeting:
Na de les op dinsdag of op vrijdag tijdens coaching les (indien nodig). Op vrijdagen kunnen we ook gelijk gebruik maken van de leraren om eventuele vragen te stellen of hulp te vragen. Plek van meeting kan binnen Kinepolis, als de zaal beschikbaar is, of in de bibliotheek.

<br />

- - - 
## Wanneer is de daily? 

<br />

### Elke dooderweekse dag vóór 15:00 uur.

<br />

### Uitwerking:
Op een online platvorm staat een agenda die ingedeeld is met alle teamleden in dit bestand staan alle projecten aangegeven op kleur en op naam van het teamlid zodat ieder makkelijk zijn of haar verhaal kan typen. Er wordt verwacht dat dit iedere dag wordt ingevuld (weekend mag eventueel). Ieder teamlid heeft een gedeelte waar hij/zij zijn/haar bezigheden of taken kan typen. Wordt dit niet goed ingevuld dan volgt er een teamoverleg om te kijken of er reden is en om vroeg tijdig te kunen zien als het niet goed gaat met een teamlid. 

<br />

- - - 
## Gevolgen niet voltooide taken en afwezigheid. 

<br />

### -Te laat komen 
### -Niet inleveren van werk.
### -Te laat inleveren van werk.
### -Afwezig bij afgesproken meetings.

<br />

Er is unaniem vastgesteld dat er gebruik wordt gemaakt van een waarschuwing methode m.b.t. de bovenstaande items. 

<br />

### Waarschuwingen (inhoud):
1x  Groep geeft een waarschuwing.
<br/>

2x  Groep gaat gezamelijk zitten om te bespreken wat er gaande is met bijzijn van docent.
<br/>

3x  Groep neemt actie met behulp van docent. 

<br/>
 


*Waarschuwingen gelden alleen indien er geen *valide verklaring* wordt gegeven. (Voorbeeld: familie/privé kwestie, gezondheid, persoonlijke zaken etc.). Er wordt wel verwacht dat hij/zij bereikbaar is m.b.t. de afwezigheid tenzij hij/zij hiervoor een *valide reden* heeft. Wordt er te vaak **dezelfde reden** gegeven dan kan dit ook leiden tot een waarschuwing.

<br/>

- - -
## Afmelden en ziekmelden. 
<br/>

### Afmelden:
Voor het afmelden hebben we een tijdsbestek van **1 week** met een *maximale* rekking van **48 uur**. Deze regel geld voor afspraken die van te voren gepland kunnen worden denk aan tandarts, dokter, fysio etc. 
<br/>

*In geval van ***nood*** kan het altijd voorkomen dat het tijdsbestek niet gehaald kan worden dan (indien mogelijk) graag **24uur** of zo snel mogelijk melden dat het desbetreffende teamlid niet aanwezig kan zijn. Natuurlijk geldt deze regel alleen als er een *valide rede is van late melding*. 

### Ziekmelden:
Voor het ziekmelden hebben we een tijdsbestek van **24uur** van tevoren. In deze corona tijden vragen wij aan alle teamleden om zijn/haar *eigen gezondheid* in acht te nemen maar natuurlijk ook die van het team. Met andere worden, *tijdens verkoudheid ziek goed uit neem voldoende rust* zodat bij terugkeer er weer **geknalt** kan worden.

*Er kan altijd bestaan dat het tijdsbestek niet gehaald kan worden dan geld ook voor deze regel dat er een *valide reden* gegeven moet worden door het teamlid voor het te laat ziekmelden.

<br/>

---
## Onderlingen afspraken.
<br/>

- Er wordt **respectvol** naar ieders mening geluisterd.
- Tijdens discussies laat ieder erlkaar **uitspreken**.
- Tijdens **belanrijke meetings** worden telefoons niet gebruikt. (Indien nodig). 
- Onderlingen **irritaties** moeten worden **aangekaart** zodat er geen obstacles binnen het team onstaan. *(Het team moet een betrouwbare en relaxte plek zijn)*.
- Elkaars privé zaken worden **gerespecteerd**. 
- Er wordt **niet** op elkaars niveau neergekeken. 

<br/>

---

## Contact.

<pre>
Naam:                   Chelly Somopawiro
Studentnummer:          1161975
E-mail school:          S1191875@student.windesheim.nl
E-mail privé:           sweet.chellys@gmail.com
Telefoonnummer:         06-39233047


Naam:                   Aman Maksoedan  
Studentnummer:          1155441
E-mail school:          S1155441@student.windesheim.nl
E-mail privé:           aman.maksoedan@gmail.com
Telefoonnummer:         06-55190414


Naam:                   Hermijn Verbaan     
Studentnummer:          1155073
E-mail school:          S1155073@student.windesheim.nl
E-mail privé:           h.verbaan@hotmail.nl    
Telefoonnummer:         06-17660028


Naam:                   Edwin Mulder 
Studentnummer:          1163468
E-mail school:          S1163468@student.windesheim.nl
E-mail privé:           edwin.mulder1999@hotmail.nl
Telefoonnummer:         06-12367561


Naam:                   Jamal Arnhem
Studentnummer:          1159101
E-mail school:          S1159101@student.windesheim.nl
E-mail privé:           jamaljjarnhem@gmail.com
Telefoonnummer:         06-30766981
</pre>
