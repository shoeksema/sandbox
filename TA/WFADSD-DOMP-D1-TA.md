# Team Afspraken WFSDAD20.VD1

Tijd contact moment: 9:00 daily standup op doordeweekse dagen dat we
geen school hebben. 1 keer per week, na school lessen, in de bieb bijeenkomen.
Op tijd aangeven als iets niet lukt, en aan elkaar om hulp vragen, zodat
je niet te laat bent met je taak.

# Te laat komen
1. **1e Keer** Word je erop aangesproken en navragen de reden
2. **2e Keer** Waarschuwing
3. **3e Keer** 2e waarschuwing
4. **4e Keer** Hele team trakteren.

# Niet inleveren werk
1. **1e Keer** Krijg je een officiele waarschuwing en navragen de reden.
2. **2e Keer** Krijg je een 2e officiele waarschuwing.
3. **3e Keer** Gesprek met hele team waarom het zo loopt en hoe we dit kunnen
verbeteren indien nodig een docent erbij halen.
4. **4e Keer** Uit het groepje gegooid.

# Te laat inleveren werk:
1. **1e Keer** Word je aangesproken door het team en navragen de reden.
2. **2e Keer** Krijg je een officiele waarschuwing.
3. **3e Keer** Krijg je een 2e officiele waarschuwing.
4. **4e Keer** Gesprek met hele team waarom het zo loopt en hoe we dit kunnen
verbeteren indien nodig een docent erbij halen.
5. **5e Keer** uit het groepje gegooid.

# Afmeldingen
Afmeldingen bij ziekte dag van te voren of in de ochtend
vroeg. Indien niet ziek minimaal 3 dagen van te voren.

# Taken
Taken verdelen om het project op tijd klaar te krijgen: + product
definitie + presentatie aan product owner en stakeholders 1. Navbar 2.
Webshop 3. Login & Register 4. Content 5. Voorraadbeheer gekoppeld aan 1
of evt. meerdere winkels 6. Blog voor reviews 7. Later Extern
betalingsysteem API + advies 8. PDF-factuur, mail naar klant +
Bestelling wordt voorbereid en naar PostNL gestuurd. Klant ontvangt een
`track and trace code`.

# Programmeer regels
Deze regels zijn in plaats met als doel on de code conform te houden.

## Taal
Benaming van variablen, functies, classes, etc. word in het Engels gedaan. Een
standaard in de IT is dat alles in het Engels word gedaan, hier houden wij ons
ook aan.

## Comments
Comment voor elke functie, in het Engels, die kort beschijft wat de functie
doet en terug geeft.

## Naam conventies
Namen voor variablen word in alles in kleine letters getypt, en elk word word
gescheiden met een `_` underscore.
```
<?php

$foo_bar = "Value"

?>
```
Namen van functies worden geschreven in [CamelCase](https://nl.wikipedia.org/wiki/CamelCase).
```
<?php

// Naming Convention example
function FooBar($foo_bar) {
  echo($foo_bar);
}

?>
```
Namen van classes is altijd een enkel woord beginnend met een hoofdletter.
```
<?php

class Foo {

}

?>
```

# Communicatie
Communicatie word gedaan via de Whatsapp Groep of de Discord server van het
team. Alle school gerelateerde dingen kunnen hier via gedeeld worden. Code
Errors of problemen kunnen hier of in de weekelijkse fysieke bijeenkomst, in de
bieb, besproken worden.

# Teamleden
Scrum rollen kennen we toe aan de teamleden na de Agile lessen.

| Team Lid          | Scrum Rol |
|-------------------|-----------|
| Jeffrey Goijaerts |           |
| Aman Ahmed        |           |
| Wybren Terpstra   |           |
| Farsan Houshiar   |           |
| Ayoub El Kaoui    |           |
