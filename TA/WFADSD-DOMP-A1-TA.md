### The A-Team 
# Samenwerkingsovereenkomst  

**Datum**: 07/09/2020  

**Studiejaar**: 2020 

**Opleiding**: (AD) Software Development 

---

### Doel van overeenkomst 
Deze samenwerkingsovereenkomst is een afspraak tussen studenten die, in het kader van hun opleiding, in groepsverband projectopdrachten uitvoeren. Alle studenten van de projectgroep dienen deze overeenkomst te ondertekenen en verklaren daardoor zo optimaal mogelijk samen te werken om een zo goed mogelijk projecteindresultaat op te leveren. 

####Deze samenwerkingsovereenkomst is geldig tot: 
* alle projectresultaten zijn opgeleverd, én 
* alle betrokkenen een beoordeling hebben ontvangen, én 
* projectleider en coach (docent) instemmen met beëindiging project. 
---

####In deze samenwerkingsovereenkomst staan beschreven: 
1. Studenten (projectleden) 
2. Afspraken 
 
---
###Studenten (De boys) 

Naam: Joeghanoe Bhatti 

Studentnummer: 1154146 

Email: s1154146@student.windesheim.nl 

Mobiel: 0657465445 
 
---
Naam: Niels Horeman 

Studentnummer: 1155810 

Email: s1155810@student.windesheim.nl 

Mobiel: 0642069411 

---
 
Naam: Alec Hoefsmid 

Studentnummer: 1153374 

Email: s1153374@student.windesheim.nl 

Mobiel: 0613994415 

--- 

Naam: Renas Khalil 

Studentnummer: 1149006 

Email: s1149006@student.windesheim.nl 

Mobiel: 0684083941 

---

Naam: Noah Bosman 

Studentnummer: 1144541 

Email: s1144541@student.windesheim.nl 

Mobiel: 0640961075 

---

##2. Afspraken  

*Hier staan alle afspraken die, los van de inhoud van het project, noodzakelijk zijn voor het goed functioneren in teamverband.*

####Werktijden 

* Shared agenda (let op hier komen de daadwerkelijke tijden) 
* Maandag tot Donderdag tussen 9 en 6 beschikbaar zijn, het liefst geen werk plannen in deze tijd. 

####Communicatie 

* Elke dag daily standup 30 minuten voor start van werk.
* Wekelijks fysiek contactmoment op maandag na de DOMP les
* Gedeelde agenda 
* Mailen aan groep via outlook. 
* Joeghanoe doet de communicatie vanuit de groep richting docenten 

####Aanwezigheid 

* Afwezigheid wordt vantevoren besproken als je afwezig bent dan is dat van tevoren besproken, dit dient bij een lange afwezigheid (meer dan 1 dag) minimaal 1 week van tevoren gemeld te worden. Bij ziekte dien je dit voor 9 uur in de ochtend gemeld te hebben via het Whatsapp kanaal 

####Reactietijden 

* Binnen 24 uur en het liefst zo snel mogelijk 
* Er wordt van je verwacht dat je binnen een termijn van 24 uur reageert, het liefst zo snel mogelijk. Als je dit op een bepaalt moment niet lukt dien je dit vroegtijdig (dag van tevoren) aan te geven. 

####Overige 
* Niet naar de master branch pushen. 
* Niet naar de develop branch pushen zonder discussie en goede reden. 
* Alle commits moeten een duidelijke beschrijving hebben van de veranderingen die er in de commit zijn gedaan. 
* Altijd een pull request maken voordat je een branch merged. 
* Syntax, werkwijze en kwaliteit volgen die in de project documentatie staat. 
* Als iets niet opgeleverd wordt op aangegeven tijd zonder enige communicatie wordt er actie ondernomen volgens het besproken plan in de eerste week.