Teamafspraken

	•	Tijd contact moment
		Elke vrijdag om 10 uur. Eventuele teams-meetings gedurende week in overleg.

	•	Afspraken bij:
	◦	Te laat komen:
		Altijd communiceren als je te laat dreigt te komen.

	◦	Niet inleveren werk / Te laat inleveren werk
		Contact opnemen met degene die het werk niet heeft ingeleverd en bespreken waarom het werk niet af is, te laat of niet ingeleverd is.
		Degene die te laat is of het werk niet inlevert moet dit van te voren laten weten zodat andere teamleden eventueel kunnen helpen.

	•	Wat verstaan jullie met op tijd afmelden?
		Zo snel mogelijk laten weten. Als het kan minimaal een dag van te voren.
		Ziek melden voor 9 uur s’ochtends.

	•	Wie zitten er in het team
		Willem König
		Daphne Zwuup
		Ronan Nieuwenhuijse
		Annemieke Kieft
		Amy Agterberg