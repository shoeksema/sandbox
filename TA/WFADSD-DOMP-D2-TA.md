#Teamovereenkomst D2

##Deelnemers: 
Naci Kaya (1160280), Jesse Folkertsma (1163142), Sam Spronk (1159678), Ewout Wieten (1107688), Roy Roman (1159208)  

##Afspraken: 
1. Kom op tijd. 
2. Afmelden doen we twee dagen van tevoren, liever eerder. 
3. Qua planning hebben we per dag kort een check wat iedereen die dag gaat doen. 
4. Per week wordt geëvalueerd of iedereen goed gewerkt heeft en wordt besproken of we als groep nog op schema liggen. Zo niet, hoe lossen we dat op.  
5. Elke dag wordt ’t logboek bijgehouden. 
6. Dinsdag tot en met vrijdag zijn we voor elkaar beschikbaar. 
7. Dinsdag en een nog later over een te komen dag, meeten we fysiek. 
8. In ‘t weekend en de maandag heb je tijd om persoonlijk dingen te regelen en ander huiswerk bij te werken. 
9. Een werkdag bestaat uit 8 uur. Vanaf 9 uur tot 5 ben je dus beschikbaar, met uitzondering van de colleges. Uitzonderingen: Naci tot 4/5 (en woensdagen oppassen tot 13:00), in verband met training. Roy op vrijdag mogelijk tot 13.00 in verband met werk.  
10. Wanneer blijkt dat wegens gebrek aan inzet werk, taken of andere afspraken niet gehaald zijn, ontstaat er een conflict en verwijzen we dus naar punt 11. 
11. In verband met conflicten, gaan de leden die wel hun afspraken nagekomen in overleg hoe ’t opgelost dient te worden. Na geen verbetering wordt er naar een coach gestapt voor een verder oplosing. Indien er met de coach nog niets verbeterd, wordt het misdragende lid uit het team verwijdert. 
