# Teamcode Groep E2 
   #####Lorenzo Colin, Mert Kirez, Jake Zweers, Alex van der Steen, Diamé Morreau

## *Inzet/werkwijze*

1. Projectleden dienen aanwezig te zijn op de afgesproken tijden en op de afgesproken plaats. 

	1. Bij een geldige reden, wordt afwezigheid getolereerd. Hieronder vallen incidentele gebeurtenissen (bijv.: ziekte, overlijden, ongelukken). Geplande gebeurtenissen (bijv.: trouwerij, begrafenis, doktersbezoek). In dit geval dient afwezigheid uiterlijk een dag van tevoren gemeld te worden. 

	1. Indien het projectlid geen geldige reden heeft om afwezig te zijn, krijgt het projectlid een waarschuwing dit zal in een algemeen Excel bestand op de ‘Google drive’ worden bijgehouden.  

2. Ieder projectlid dient goed bereikbaar te zijn.  

	2. Projectleden hebben elkaars telefoonnummer en e-mailadressen en kunnen zodoende elkaar bereiken. 

	2. Het is toegestaan om niet bereikbaar te zijn, mits hier een goede reden voor is. 

        *(Enkele goede redenen zijn: stroomuitval, werk, ziekte, familieaangelegenheden, geen bereik/ontvangst. Mocht hier beroep op worden gedaan behoudt de scrummaster het recht een bewijs aangeleverd te krijgen. Denk hierbij aan nieuwsberichten, werkroosters, brief van dokter of een ander aantoonbaar hard bewijs.)*  

    2. Projectleden dienen van 9 uur ’s ochtends, tot 6 uur ’s avonds bereikbaar te zijn. (doordeweeks) 

3. Ieder projectlid dient de gemaakte afspraken na te komen. 

	3. Indien het projectlid geen geldige reden heeft om afspraken niet na te komen, krijgt het projectlid een waarschuwing. 

	3. Bij het niet behalen van een deadline dient dit tijdig aangegeven te worden, hiervoor is besloten dit 48 uur voor de deadline aan te geven. In overleg met de groep kan worden besloten de deadline te verkorten. 

	3. Bij overmacht waarbij het projectlid totaal niet in staat (ernstige ziekte e.d.) is om de afspraak na te komen, wordt het zo goed als mogelijk opgevangen door de overige projectleden. 

4. Ieder projectlid dient een evenredig deel van het werk te doen. Meeliftgedrag wordt niet getolereerd. 

	4. Bij overmacht waarbij het projectlid totaal niet in staat (ernstige leerstoornissen, ernstige ziekte e.d.) is om het werk na te komen, wordt het zo goed als mogelijk opgevangen door de overige projectleden. 

	4. Bij sprake van meeliftgedrag zal een gesprek worden aangegaan met de scrum master/coördinator en de projectlid in kwestie. Mocht na dit gesprek er geen voortgang worden geboekt volgt per keer dat de projectlid hierop aangesproken wordt een waarschuwing. 

	4. Projectleden dienen elkaar aan te spreken op het nakomen van afspraken. 

5. Ieder projectlid dient zijn eigen gewerkte uren bij te houden.  

	5. Het bijhouden van de uren dient een projectlid  dagelijks in zijn/haar logboek te doen. Hierin beschrijft een projectlid ook welke taken hij heeft gedaan en enige opmerkingen hierbij. 

	5. Uren mogen beide in of buiten school om gemaakt worden. 

## *Waarschuwingen* 

1. Bij het overtreden van één van de bovengenoemde punten, ontvangt het projectlid een waarschuwing van de scrum master. (Met de groep) 

	1. Elke waarschuwing wordt besproken in de groep. 

	1. In de daaropvolgende week worden de vorderingen besproken. 

	1. Indien de desbetreffende persoon een derde waarschuwing ontvangt, wordt er contact opgenomen met de SLB’er vanwege consequenties. En is er de mogelijkheid om in overleg met de SLB’er om deze persoon uit de groep te zetten. 

2. **10 minuten inlooptijd**, mits anders aangegeven.

3. Waarschuwing bij deadline als het niet af is, mits van te voren aangegeven.

4. Als je ziek bent, voordat de les begint, behoort dat aangegeven te worden.

5. Als iemand te vaak te laat komt, zelfs met een geldige reden, behoudt de groep het recht om mensen daarvoor een waarschuwing te geven.

*Ondertekening teamleden*
![Handtekening Leden](/assets/Handtekeningleden.png)