### Teamafspraken B3 ###

---
Groep: B3

Leden: Roderik Schiele, Trisjan Mustamu, Houwaida El Amrani, Maksim Hofker, Ali zadeh Saviz

##### Groep regels en afspraken #####
1. Afgesproken is dat we elke maandag en vrijdag na de les naar de bibliotheek in Almere gaan als vaste fysieke afspraak. De rest van de afspraken zal via  Microsoft Teams lopen. Een eventuele extra fysieke afspraak naast de twee eerder genoemde kan ook op maandag besloten worden, afhankelijk van de voortgang van het project.
  
2. De werkende groepsleden zullen hun werkrooster elke zondag doorgeven zodat we op basis daarvan de volgende afspraken kunnen inplannen.

3. In een uitzonderlijke situatie waarbij een groepslid niet aanwezig kan zijn, dient het lid zich op tijd af te melden. Met op tijd afmelden verstaan we minimaal 3 uur van te voren.

4. Indien een groepslid niet op tijd bij de fysieke en/of online afspraken aanwezig kan zijn, verwachten we dat dit voor de afgesproken tijd doorgegeven is, zodat de leden niet op een lid hoeven te wachten. Dit is vooral van toepassing in het geval van vertraging van openbaar vervoer of drukte/files op wegen. 

5. Deze groep heeft afgesproken dat er zelf een deadline ruim voor het inlevermoment wordt bepaald zodat de groepsleden voldoende tijd hebben om de opdracht in zijn geheel te kunnen bespreken en goed te keuren. 
   Vervolgens is er afgesproken om in het geval van vastlopen, dit ruim van tevoren te melden zodat hier een andere oplossing voor kan gevonden worden. Onder oplossing denken we aan de groepslid hint geven, uitleggen en ondersteunen enzovoort. Dit geeft ons ook de gelegenheid om op tijd een afspraak met docenten in te kunnen plannen

6. Bij het te laat inleveren zal als eerst het teamlid hierover gewaarschuwd worden en indien dit voor de tweede keer gebeurt, zijn we genoodzaakt dit bij docenten te melden. Wij zullen hier wel rekening houden met de uitzonderlijke situaties (o.a. ziekte, crematie enzovoort) waar het teamlid geen controle over heeft.

7. Het totaal niet inleveren van een opdracht zonder dat vantevoren te melden en zonder geldige reden betekend voor ons allemaal ‘Klaar’ en dan zijn we genoodzaakt om dit gelijk aan de docenten te melden.
## ##
##### Project WFFlix #####
##### Opdracht 1: Basisopzet project definitie #####
1.	Titelblad met o.a. project naam, subtitel, datum, auteur, versienummer en management samenvatting met daarbij een inhoudsopgave. (Roderik)
2.	Aanleiding en bedrijf/project organisatie binnen een businesscase. (missie) (Trisjan)
3.	Product visie (Ali)
4.	Projectgrenzen en randvoorwaarden (Ali)
5.	Vraagstelling (Houwaida)
6.	Probleemstelling (Houwaida)
7.	Doelstellingen (Maksim)
## ##


