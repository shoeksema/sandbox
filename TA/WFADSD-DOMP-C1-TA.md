#Teamafspraken

##Contact moment inclusief Daily standup:
- Maandag: 10:00 t/m 11:00
    - Bespreking over taakverdeling
- Dinsdag: 16:00 t/m 17:00
- Woensdag: 15:30 t/m 16:30
- Donderdag: 12:30 t/m 13.30
- Vrijdag: 10:00 t/m 15:00
    - Fysieke meeting (Samenwerken)
- Zaterdag en zondag: Wanneer nodig.
- Tijdens de daily stand ups wordt er besproken hoe lang we nodig hebben voor een taak en dit wordt genoteerd.

##Afspraken bij:
- Te laat komen of niet komen opdagen:
    - Altijd communiceren.
    - Bij niet communiceren dan volgt er een waarschuwing. Bij de tweede keer wordt er een e-mail naar de product owner (Rudy) gestuurd.
- Niet inleveren werk / Te laat inleveren werk:
    - Als iemand te laat dreigt te komen met zijn taak, dan is het noodzaak om tijdens het eerstvolgende daily standup te laten weten zodat andere kunnen inspringen.
    - Bij het niet inleveren van werk zonder dit van te voren te hebben gecommuniceerd dan volgt er een waarschuwing. Bij de tweede keer wordt er een e-mail naar de product owner (Rudy) gestuurd.

##Wat verstaan jullie met op tijd afmelden?
- Minimaal een dag van te voren.
- Ziek melden voor 9 uur s’ochtends.

##Wie zitten er in het team
- Willem König
- Daphne Zwuup
- Ronan Nieuwenhuijse
- Annemieke Kieft
- Amy Agterberg