Teamafspraken
---
####Het team:
Team A3 bestaat uit de volgende personen:
-	Awin Daliri
-	Danjel Schoew
-	Donny den Heeten
-	Nigel Overman
-	Stefan Maas
-	Toprak Akgür

####Samenwerking / Contactmomenten: 
Er zal gebruik gemaakt worden van dagelijkse stand-ups om te melden wat persoonlijk is bijgedragen aan het project. Dit wordt algemeen gedaan aan het einde van de werkdag.

Woensdagen en Vrijdagen zijn de vaste dagen waarop wij gezamenlijke team gesprekken houden ter overleg van de uitgevoerde, en nog ingeplande werkzaamheden (min. Vanaf 11:00, max. Tot 17:00). 

####Communicatie: 
Voor communicatie worden Whatsapp, Outlook en Microsoft Teams gebruikt.
Code wordt gedeeld op Gitlab.

####Rolverdeling:
Zodra een project van start gaat wordt er gezamenlijk gekeken naar wie welk onderdeel doet, zodat dit eerlijk verdeeld kan worden. Het is niet de bedoeling dat iemand elk project dezelfde taken op zich neemt. Er wordt dus verwacht dat er met nieuwe projecten afwisseling plaatsvindt in wie de verschillende taken oppakt. 
 
Mocht jouw werk eerder af zijn dan die van een ander? Vraag dan aan elkaar of het oké is om te beginnen aan een nieuwe taak, of help een teamlid met zijn deel, mocht hij ergens moeite mee hebben.

Als het niet lukt om een afgesproken deel op tijd af te maken, wordt verwacht dat dit alvorens aangegeven wordt bij het team.

####Werkmomenten: 
Buiten de lesuren om wordt verwacht wekelijks 32 uur te spenderen aan studiewerk. Hierbij wordt dus ook verwacht dat
deze uren worden gebruikt voor projecten.

In de onderstaande tabel staat aangegeven hoeveel uur per dag iemand tijd heeft om te werken aan zijn taken. 
De aangegeven uren zijn exclusief lesuren, en weekenden worden gepland naar eigen wens.

>|         | Ma| Di|  Wo| Do| Vr| Za| Zo|
>|---      |---|---|--- |---|---|---|---|
>| Awin    | 6 | 5 | 10 | 8 | 5+| - | - |
>| Danjel  | 6 | 8 | 6  | 6 | 6 | - | - |
>| Donny   | 6 | 6 | 8  | 6 | 6 | - | - |
>| Nigel   | 5 | 8 | 3  | 5 | 8 | - | - |
>| Stefan  | 5 | 8 | 8  | 6 | 8 | - | - |
>| Toprak  | 6 | 6 | 8  | 6 | 6 | - | - |

>*(Dit is slechts een geschatte indicatie van beschikbare uren. Roosters kunnen per periode verschillen, en overige studie momenten zijn
>hier niet in meegeteld.)*

####Inlevermoment:
Projecten dienen een dag voor het inlevermoment afgerond te zijn, zodat alles nog gezamenlijk doorgekeken kan worden met het team en het project eventueel al ingeleverd kan worden. Werk moet uiterlijk om 21:00 van de einddatum ingeleverd zijn via het aangegeven inleverpunt (zoals bijvoorbeeld ELO). 


####Wat als een project niet af is:
Er zal een duidelijke planning en verdeling zijn om dit te voorkomen. Mocht dit niet zijn gelukt?:
-	Wat is er fout gegaan?
-	Waarom is dit fout gegaan?
Het gehele team is verantwoordelijk om de vooruitgang bij te houden en elkaar hierop te wijzen. Mocht er een achterstand zijn en het inlevermoment breekt aan, wordt er verwacht ingeleverd te worden wat wel is gemaakt.

####Wat als een persoonlijk deel niet (op tijd) af is: 
Als iemand zijn werk, zonder goede reden, niet af heeft zal het team eerst gezamenlijk een waarschuwing geven aan de desbetreffende persoon. Dit wordt gedaan via e-mail, met de rest van het team in de CC. 

Bij het tweede incident wordt dit aangekaart bij één van de docenten, om hier gezamenlijk een oplossing voor te vinden.

Na drie keer zal de persoon worden geëxcommuniceerd van het team. Het team zal hierbij de overige taken onderling verdelen, om met het project geen achterstand op te lopen. 

Mocht iemand zijn werk *te laat* inleveren, ook zonder goede reden, zullen dezelfde stappen genomen worden. Als iemand zijn werk niet af heeft gekregen voor de inlever deadline en dit niet vroegtijdig heeft aangegeven, dan wordt dit direct doorgeschakeld naar de docent(en). Het team is alsnog verantwoordelijk voor het eindresultaat 

Als hier wel goede redenen voor zijn, zal onderling overlegt worden hoe wij dit kunnen oppakken en voorkomen.


####Verzuim:
Er wordt verwacht een dag van te voor aan te geven wanneer je niet aanwezig kan zijn voor een afspraak. Mocht dit niet lukken dan kan dit maximaal tot en met 10:00 ’s ochtends worden aangegeven.
Er wordt echter wel verwacht dat er doorgewerkt wordt aan jouw aangegeven onderdelen. 
Mocht dit niet lukken, en er is geen goede reden voor, dan worden de bovenstaande stappen herhaald:
-	Eerste keer: Waarschuwing via e-mail.
-	Tweede keer: Aankaarten bij docent.
-	Derde keer: Excommunicatie van het team.

Is het vanwege legitieme omstandigheden niet goed mogelijk een deel af te maken tijdens jouw afwezigheid? Dan zal ook dit op tijd aangegeven moeten worden, zodat het team dit kan overnemen om achterstand te voorkomen.

\
Deze afspraken zijn bedoeld voor een soepele samenwerking, met als doel een 
professionele en prettige werkomgeving. 
