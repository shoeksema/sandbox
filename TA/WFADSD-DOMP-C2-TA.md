Team leden; 

David Peetoom, David van Dijk, Ahmet Solak, Simon-Jan Engele & Chantalle den Hertog 

 

Bij 3 officiële waarschuwingen bespreken met de coach 

Bij 3 groeps waarschuwingen 1 officiële waarschuwing 

 

 

Contact moment: 

 

Dinsdag middag van 12:30 tot 17:30 in de bibliotheek 

Teams meeting: 

Maandag 9:00 tot 17:00 

Vrijdag 9:00 tot 17:00 (8:30 tot 12:30 coach) 

Elke maandag terugkoppeling naar wat er die week afgesproken is. En kijken waar iedereen tegen aanliep en eventueel hulp aan elkaar bieden. 

Vrijdag kijken hoe jouw onderdeel er voor staat en communiceren 

Bij aanmelding van een extra meeting ben je ook daadwerkelijk aanwezig 

 

Te laat komen 

OV vertraging -> appen of bellen -> groeps waarschuwing als je niks van je laat horen 

Verslapen -> laten weten 

meer dan 3 keer in de week of 5 keer in de maand te laat officiële waarschuwing 

Bij afspraak vergeten -> alsnog komen of via teams. 

Als je helemaal niet kan door andere plannen -> groeps waarschuwing 

 

Afmelden 

Ziek melden 

Als je het een dag van te voren weet een dag van te voren melden 

Als je het voelt aankomen meteen laten weten 

Anders s’ochtends afmelden  

Wanneer fysiek afgesproken wordt en iemand is ziek/afwezig (corona, verkoudheid verschijnselen) die woont de meeting bij met teams 

Persoonlijke afspraken zodat de rest weet dat je niet bereikbaar bent 

1 week van te voren 

 

 

Inlever moment: 

Niet 1 dag voor het officiële inlever moment inleveren maar 3 dagen! -> Anders uit het team! 

 

 

Niet inleveren van werk: 

Indien moeite met de opdracht; laat je team op tijd weten zodat zij kunnen helpen. Niet 2/3 dagen voor inlever moment -> officieel waarschuwing 

Niet inleveren van werk zonder toelichting een groepswaarschuwing. 

Als een document niet wordt aangeleverd dan wordt het document de week erop aangeleverd plus de documenten van de huidige week 

 

 

Rekening houden met: 

Bij chronische ziekte wordt er direct contact opgenomen met de coaches 

 

 

 

Unaniem eens over een extreme geval: 

Direct contact opnemen met coach 

 

Deze regels gelden per periode 

 

We helpen elkaar altijd 