# Samenwerkingsovereenkomst team A2
---

Datum: 08-09-2020 | Plaats: Almere

| Lid 1 | Lid 2 | Lid 3 | Lid 4   | Lid 5 |
|-------|-------|-------|---------|-------|
|Hikmet | Ilias | Jorai | Kristof | Scott | 
---
# regels:

1. **samenwerken**

    - We maken meetings (via teams) om te overleggen en om samen te werken via teams. 

2. **communicatieregels**

    - Minstens eenmaal daags de schoolmail controleren, en indien nodig reageren.
    - Meetings plannen we in via Microsoft Teams
    - Afwezigheid bij de meeting melden we minstens 2 uur van te voren via de Teams chat.

3. **ongeoorloofd afwezigheid**

    - houdt in, afwezig zonder aantoonbare communicatie met minimaal 1 teamlid met een duidelijke en/of geldige reden.

        __WORDT BESTRAFT MET EEN STRIKE__

4. **Afspraken over deadlines**

    - We spreken als team een gezamenlijke deadline af waar we ons strikt aan houden.
    
5. **Werk niet of zonder geldige reden?**

    - Dit wordt bestraft met een strike

6. **wat te doen als je je werk niet op tijd afkrijgt?**

    - Vraag op tijd om hulp als je vastloopt.
    - Vraag om hulp als je meer tijd nodig hebt.
    
         _wordt er niks gecommuniceert en heb je je werk niet af dan wordt regel 5. gehanteerd_

7. **Wat gebeurt er als je een STRIKE  krijgt?**

    1. Duidelijke waarschuwing.
    2. een extra opdracht waardoor je meer tijd moet besteden aan het project.
    3. Uitsluiting uit het team.
    
        __elke strike wordt gecommuniceerd met de leraren__

### *enkel en alleen wanneer de meerderheid in de groep vindt dat een regel niet van kracht dient te zijn omwille van een onvoorziene situatie bij een van de teamleden zal er geen bestraffing volgen*

<br>
Ondertekening:
Verbale akkoord door alle teamleden

