Teamafspraken:

Teamleden:

- Joshua
- Noah
- Jonathan
- Erwin
- David
- Killyan

1. Te laat komen zonder goede reden

   - 1x, kan gebeuren.
   - 2x, interventie met elkaar, bespreken wat er gebeurd en hoe het opgelost
   - kan worden.
   - 3x, docent erbij betrekken.
   - 4x, hebben we de optie om de samenwerking te stoppen.

2. Ziek zijn
   - Afmelden voor 09.00 uur.
   - Ziek zijn is ziek, alleen je moet wel fanatiek achter gemiste lessen aangaan, en vanaf thuis wel proactief inzet tonen.
   - Ziekmelden via de projectgroepsapp .
   - Als iemand langer dan 2 weken ziek is (zonder doktersverklaring), dan hebben we de optie om de samenwerking te stoppen.
3. Eventueel stoppen opleiding

   - Bespreek het op tijd, geef je taken eventueel door aan een ander.

4. Meetings

   - 2x in de week via Discord, of real life (woensdag & vrijdag 09.30u).
   - Tot zo lang het nodig is.

5. Niet inleveren werk
   - Op tijd hulp vragen.
   - 1x waarschuwing, gelijk ook bespreken.
   - 2x studiecoach erbij halen.
   - 3x optie om samenwerking te verbreken.
