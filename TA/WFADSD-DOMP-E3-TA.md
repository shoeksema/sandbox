# Team Afspraken 

**Team:**

Boyd Appiah

Vito Ledchumanan

Yekta Uysal

Rifati Huseni

Micheal Leschot

**Onze Meeting moment:**

Dinsdag: 15:00 Microsoft Teams

Woensdag: 20:00 Microsoft Teams

Vrijdag: 08:30 - 12:30 coaching

Zaterdag: 15:00 Microsoft Teams

Zondag: 15:00 Microsoft Teams

**Ziek zijn en afspraken:**

Als je ziek bent, meldt het via whatsapp in de groepschat. Liefst 15 minuten voor de meeting of dezelfde ochtend van de afspraak. Heb je iets dergelijks als ziekenhuis of huisarts afspraken meld het dan alvast een paar dagen voor de afspraak, zodat het geen problemen oplevert.

**Te laat:**

Bij meetings mag je maximaal 15 minuten te laat komen en als je te laat komt bij coaching op vrijdag door het openbaar vervoer meldt dit dan via whatsapp. Als je niet komt of je laat niets van je horen, krijg je een waarschuwing. Bij twee waarschuwingen wordt het gemeld bij de coach.

**Werk inleveren:**

Iedereen moet zijn werk optijd inleveren bij elo. Als je twijfelt ga dan voor de zekerheid nogmaals kijken of je de opdacht die moest worden ingeleverd, hebt ingelevert. Als het niet lukt met inleveren, neem dan contact op met de coach of klasgenoten en kijk of ze je kunnen helpen, anders moet je het herkansen.


