# **Groepsregels** 
 
Dit zijn de groepsregels voor team E1. Met deze groepsregels 
willen wij een goeie samenwerking te gemoed gaan. 

na 6 weken vervallen de waarschuwing de uitgedeelt zijn 
<br></br> 

## *Teamleden*
Jelmer Lam,
Jurgen Prins,
Carlo Neuteboom,
Jeroen Boxhoorn,
Ryan Zuiver.

<br></br>
## *Aanwezig* 
* Zo snel mogelijk melden liefst 2 weken van tevoren. Bij niet melden van afwezigheid een officiële waarschuwing. 
* Maandag na school gaan we bij elkaar zitten in de bieb om 1 uur. 
* Andere momenten zijn online en niet vast te leggen door het steeds aangepaste rooster.  
* Zonder geldige reden te laat komen is een waarschuwing
 
<br></br> 
## *Inlevermoment* 
* De dag voor het inlever in de ochtend versturen zodat er nog 1 dag is om samen te voegen. 
* Vroegtijdig aangeven als je jouw taak van de sprint niet redt voor het inlevermoment. 
 
<br></br> 
## *Wat als iemand een deel van zijn opdracht niet heeft gemaakt? * 
* 2 officiële waarschuwing via de mail voordat er een gesprek komt met de scrummaster. 

<br></br>
![Alt Text](https://in03.hostcontrol.com/resources/5c319b8fd5f2b4/71fa9f3469.JPEG)