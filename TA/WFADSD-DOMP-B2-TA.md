**Groep B2: Robin Doorenbosch, Sam broek, Micha glans, Roy meijer en
Suzanne Distelbrink**

1.  Als je een afspraak hebt, of te laat bent, meld het in de groepschat
    > z.s.m (In Teams of Whatsapp). Maximaal 2 uur van tevoren.

    a.  Vrijdag hoeft het niet 2 uur van tevoren gemeld te worden als we
        > met een docent willen spreken.

2.  Als er iets persoonlijk aan de hand is, meld het en dan bedenken we
    > samen voor een oplossing.

3.  Elke meeting beginnen met een stand-up meeting.

4.  Heb je een vraag, vraag het gewoon aan je teamgenoten en/of de
    > docenten.

5.  Het is niet erg als je een deadline niet heb gehaald, als je er maar
    > wat tijd aan hebt besteed.

6.  Te laat komen/te laat inleveren van werk: eerst gesprek met waarom
    > je te laat was.

    a.  Als het meerdere keren wordt herhaald kijken of we het kunnen
        > oplossen

    b.  Als het daarna nog voorkomt dat de persoon te laat is de docent
        > erbij te halen.

    c.  Als dit niet helpt dan eindgesprek met consequenties.

7.  Niet inleveren werk : drie punten systeem. Bij 3 keer niks inleveren
    > komt het eindgesprek met consequenties.

**[Project planning \[ Voor week 37 t/m 44\]:]{.ul}**

  **Maandag (15.00/15.30 beginnen)**   Werk aan Flevosap.
  ------------------------------------ ------------------------------
  **Dinsdag (15.30 beginnen)**         Werk aan Flevosap.
  **Woensdag (14.00 beginnen)**        Werk aan overige opdrachten.
  **Donderdag (12.30 beginnen)**       Werk aan DOMP.
  **Vrijdag (15.00 beginnen)**         Werk aan DOMP.

**[Handtekeningen teamleden:]{.ul}**

![](media/image3.png){width="2.1093755468066493in"
height="1.0621675415573053in"}![](media/image2.png){width="2.1341590113735784in"
height="1.0625in"}![](media/image5.png){width="1.8802088801399826in"
height="0.9353794838145232in"}![](media/image4.png){width="2.744792213473316in"
height="1.3677744969378827in"}![](media/image1.png){width="1.5729166666666667in"
height="0.7864588801399826in"}
