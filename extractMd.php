<?php
/**
 * Created by: Stephan Hoeksema 2020
 * sandbox
 */

require 'parsedown/Parsedown.php';
$parsedown = new Parsedown();

$filename = $_GET['file'];


$text = file_get_contents($filename);

echo
'<form>
 <input class="btn btn-primary" type="button" value="Go back!" onclick="history.back()">
</form>';

echo $parsedown->text($text);