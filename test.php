<?php
/**
 * Created by: Stephan Hoeksema 2020
 * sandbox
 */

$a = 5;
$b = 5;
$c = $a**2;
var_dump($a++);
var_dump(--$b);
var_dump($c);

$grades = [
    'Peter' => 6,
    'Jane' => 7,
    'Paul' => 5.5
];

var_dump(array_sum($grades));